# Installation
Download this repo and run this commands: `python setup.py install` then `python setup.py build`.
# Usage
* For Text2Binary:

    `binbir e yourtext`
* For Binary2Text:

    `binbir d yourbinary`
