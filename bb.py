from sys import argv
def main():
    if argv[1] == 'e':
        print(''.join([bin(ord(c))[2:].zfill(8) for c in ' '.join(argv[2:])]))
    elif argv[1] == 'd':
        d = [' '.join(argv[2:])[i:i+8] for i in range(0, len(' '.join(argv[2:])), 8)]
        print(''.join([chr(int(x, 2)) for x in d]))
if __name__ == '__main__':
    main()
