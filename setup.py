from setuptools import setup
setup(
    name='binbir',
    version='0.1',
    description='Text to bin, bin to text converter.',
    license='MIT',
    author='mhlkan',
    py_modules=['bb'],
    url='https://github.com/mhlkan/binbir',
    entry_points={
        'console_scripts': [
            'binbir = bb:main'
        ]
    }
)
